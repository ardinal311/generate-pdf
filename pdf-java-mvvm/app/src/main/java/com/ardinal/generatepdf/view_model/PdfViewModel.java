package com.ardinal.generatepdf.view_model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.ardinal.generatepdf.model.entity.PdfContent;
import com.ardinal.generatepdf.repository.PdfRepository;

public class PdfViewModel extends AndroidViewModel {

    private PdfContent response;

    public PdfViewModel(@NonNull Application application, String title, String desc) {
        super(application);

        Boolean titleStatus = getStatus(title);
        Boolean descStatus = getStatus(desc);

        if (titleStatus && descStatus) {
            PdfRepository repository = new PdfRepository();
            this.response = repository.getPdfObject(title, desc);
        }
    }

    public Boolean getStatus(String text) {
        return !text.isEmpty();
    }

    public PdfContent getPdfData() {
        return response;
    }
}
