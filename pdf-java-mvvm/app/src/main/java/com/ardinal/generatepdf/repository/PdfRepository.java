package com.ardinal.generatepdf.repository;

import com.ardinal.generatepdf.model.entity.PdfContent;

public class PdfRepository {

    private PdfContent content;

    public PdfContent getPdfObject(final String title, final String desc) {
        if (!title.isEmpty() && !desc.isEmpty()) {
            this.content = new PdfContent(title, desc);
        }
        return content;
    }
}
