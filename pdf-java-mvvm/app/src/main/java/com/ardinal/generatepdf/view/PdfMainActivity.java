package com.ardinal.generatepdf.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ardinal.generatepdf.R;
import com.ardinal.generatepdf.model.entity.PdfContent;
import com.ardinal.generatepdf.view_model.PdfViewModel;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class PdfMainActivity extends AppCompatActivity {

    private PdfViewModel viewModel;
    private TextInputEditText titleField;
    private TextInputEditText descField;

    public PdfMainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getStoragePermission();

        getSupportActionBar().setTitle(getString(R.string.generate_pdf));

        titleField = findViewById(R.id.pdf_title);
        descField = findViewById(R.id.pdf_desc);

        Button pdfLink = findViewById(R.id.pdf_button);
        pdfLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewPdf();
            }
        });
    }

    private void getStoragePermission() {
        ArrayList<String> permission = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (!permission.isEmpty()) {
                ActivityCompat.requestPermissions(this, permission.toArray(new String[permission.size()]), 1);
            }
        }
    }

    public String getPdfTitle() {
        return Objects.requireNonNull(titleField.getText()).toString();
    }

    public String getPdfDesc() {
        return Objects.requireNonNull(descField.getText()).toString();
    }

    private void createNewPdf() {
        viewModel = new PdfViewModel(this.getApplication(), getPdfTitle(), getPdfDesc());

        if (!viewModel.getStatus(getPdfTitle())) {
            setFieldError(titleField, "Title Masih Kosong!");
        }
        if (!viewModel.getStatus(getPdfDesc())) {
            setFieldError(descField, "Deskripsi Masih Kosong!");
        }
        if (viewModel.getPdfData() != null) {
            generateThePdf(viewModel.getPdfData());
        }
    }

    private void setFieldError(TextInputEditText field, String message) {
        field.setError(message);
    }

    private void generateThePdf(PdfContent content) {
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/JAVA_PDF/";
        File dirFile = new File(dirPath);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }

        String targetPdf = dirPath + content.getTitle() + ".pdf";
        File filePath = new File(targetPdf);
        Document doc = new Document();

        try {
            FileOutputStream stream = new FileOutputStream(filePath);
            PdfWriter.getInstance(doc, stream);
            doc.open();

            doc.addTitle(content.getTitle());
            Font largeBold = new Font(Font.FontFamily.COURIER, 24, Font.BOLD);
            doc.add(new Paragraph(content.getTitle(), largeBold));
            doc.add(new Paragraph(content.getDesc()));
            doc.close();

            showMessage("JAVA MVVM PDF GENERATED!!");
        }
        catch (IOException e) {
            e.printStackTrace();
            showMessage(e.toString());
        } catch (DocumentException e) {
            e.printStackTrace();
            showMessage(e.toString());
        }
    }

    private void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
