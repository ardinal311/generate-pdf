package com.ardinal.generatepdf

import com.ardinal.generatepdf.model.PdfContent

internal class GeneratePdfPresenter(private val contract: GeneratePdfContract, private val interactor: GeneratePdfInteractor) :
    GeneratePdfInteractor.OnContentCreated {

    fun validateData(title: String, desc: String) {
        interactor.validate(title, desc, this)
    }

    override fun onTitleError() {
        contract.setTitleError()
    }

    override fun onDescError() {
        contract.setDescError()
    }

    override fun onSuccess(title: String, desc: String) {
        contract.showData(PdfContent(title, desc))
    }
}
