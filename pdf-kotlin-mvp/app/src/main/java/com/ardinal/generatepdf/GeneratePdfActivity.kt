package com.ardinal.generatepdf

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.ardinal.generatepdf.model.PdfContent
import com.itextpdf.text.Document
import com.itextpdf.text.DocumentException
import com.itextpdf.text.Font
import com.itextpdf.text.Paragraph
import com.itextpdf.text.pdf.PdfWriter
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class GeneratePdfActivity : AppCompatActivity(), GeneratePdfContract {

    private var presenter: GeneratePdfPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getStoragePermission()

        supportActionBar?.title = getString(R.string.generate_pdf)
        presenter = GeneratePdfPresenter(this, GeneratePdfInteractor())

        pdf_button.setOnClickListener {
            validateData()
        }
    }

    private fun getStoragePermission() {
        val permission = ArrayList<String>()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (permission.isNotEmpty()) {
                ActivityCompat.requestPermissions(this, permission.toTypedArray(), 1)
            }
        }
    }

    override fun validateData() {
        presenter?.validateData(pdf_title.text.toString(), pdf_desc.text.toString())
    }

    override fun setTitleError() {
        pdf_title.error = "Title Masih Kosong"
    }

    override fun setDescError() {
        pdf_desc.error = "Desc Masih Kosong"
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showData(content: PdfContent) {
        val dirPath = Environment.getExternalStorageDirectory().absolutePath + "/KOTLIN_PDF/"
        val dirFile = File(dirPath)

        if (!dirFile.exists()) {
            dirFile.mkdirs()
        }

        val targetPdf = dirPath + content.title + ".pdf"
        val filePath = File(targetPdf)

        val doc = Document()

        try {
            val stream = FileOutputStream(filePath)
            PdfWriter.getInstance(doc, stream)
            doc.open()

            doc.addTitle(content.title)
            val largeBold = Font(Font.FontFamily.COURIER, 24f, Font.BOLD)
            doc.add(Paragraph(content.title, largeBold))
            doc.add(Paragraph(content.desc))
            doc.close()

            showMessage("KOTLIN MVC PDF GENERATED!!!")

        } catch (e: IOException) {
            e.printStackTrace()
            showMessage(e.toString())
        } catch (e: DocumentException) {
            e.printStackTrace()
            showMessage(e.toString())
        }
    }
}
