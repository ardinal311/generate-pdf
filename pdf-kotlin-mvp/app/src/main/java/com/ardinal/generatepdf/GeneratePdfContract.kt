package com.ardinal.generatepdf

import com.ardinal.generatepdf.model.PdfContent

interface GeneratePdfContract {

    fun validateData()

    fun setTitleError()

    fun setDescError()

    fun showData(content: PdfContent)

    fun showMessage(message: String)
}
