package com.ardinal.generatepdf.model

data class PdfContent (
    var title: String? = null,
    var desc: String? = null
)
