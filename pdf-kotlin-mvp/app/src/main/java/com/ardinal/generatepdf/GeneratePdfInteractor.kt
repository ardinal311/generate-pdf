package com.ardinal.generatepdf

class GeneratePdfInteractor {

    interface OnContentCreated {
        fun onTitleError()
        fun onDescError()
        fun onSuccess(title: String, desc: String)
    }

    fun validate(title: String, desc: String, listener: OnContentCreated) {
        if (title.isEmpty() || title == "") {
            listener.onTitleError()
        }
        if (desc.isEmpty() || desc == "") {
            listener.onDescError()
        }
        if (title.isNotEmpty() && desc.isNotEmpty()) {
            listener.onSuccess(title, desc)
        }
    }
}
