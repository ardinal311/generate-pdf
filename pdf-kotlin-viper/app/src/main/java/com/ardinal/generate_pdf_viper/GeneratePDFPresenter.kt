package com.ardinal.generate_pdf_viper

import com.ardinal.generate_pdf_viper.model.PdfContent

class GeneratePDFPresenter : GeneratePDFContracts.GeneratePDFViewToPresenterInterface,
    GeneratePDFContracts.GeneratePDFInteractorToPresenterInterface {

    override var view: GeneratePDFActivity? = null
    override var interector: GeneratePDFInteractor? = null
    override var router: GeneratePDFRouter? = null

    override fun setTitleError() {
        view?.setTitleError()
    }

    override fun setDescError() {
        view?.setDescError()
    }

    override fun createObjectSuccess(content: PdfContent) {
        view?.generatePdf(content)
    }
}
