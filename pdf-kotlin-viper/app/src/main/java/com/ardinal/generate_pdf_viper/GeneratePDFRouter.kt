package com.ardinal.generate_pdf_viper

import android.annotation.SuppressLint

class GeneratePDFRouter : GeneratePDFContracts.GeneratePDFPresenterToRouterInterface {

    companion object {
        @SuppressLint("StaticFieldLeak")
        var view: GeneratePDFActivity? = null

        fun configure(activity: GeneratePDFActivity) {
            val presenter = GeneratePDFPresenter()
            val interactor = GeneratePDFInteractor()
            val router = GeneratePDFRouter()

            activity.presenter = presenter
            presenter.view = activity
            presenter.router = router
            presenter.interector = interactor
            interactor.presenter = presenter
            view = activity
        }
    }
}