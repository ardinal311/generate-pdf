package com.ardinal.generate_pdf_viper.model

data class PdfContent (
    val title: String?,
    val desc: String?
)