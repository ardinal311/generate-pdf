package com.ardinal.generate_pdf_viper

import android.annotation.SuppressLint
import android.content.Context
import com.ardinal.generate_pdf_viper.model.PdfContent

object GeneratePDFContracts {

    interface GeneratePDFPresenterToViewInterface {
        val context: Context
        var presenter: GeneratePDFPresenter?

        fun setupView()
        fun setupLink()

        fun setTitleError()
        fun setDescError()
        fun generatePdf(content: PdfContent)
        fun showMessage(message: String)
    }

    interface GeneratePDFPresentorToInteractorInterface {
        var presenter: GeneratePDFPresenter?
        fun validateTheData(title: String, desc: String)
    }

    interface GeneratePDFInteractorToPresenterInterface {
        fun setTitleError()
        fun setDescError()
        fun createObjectSuccess(content : PdfContent)
    }

    interface GeneratePDFViewToPresenterInterface {
        var view: GeneratePDFActivity?
        var interector: GeneratePDFInteractor?
        var router: GeneratePDFRouter?
    }

    interface GeneratePDFPresenterToRouterInterface {
        companion object {
            @SuppressLint("StaticFieldLeak")
            var view: GeneratePDFActivity? = null

            fun configure() {}
        }
    }
}
