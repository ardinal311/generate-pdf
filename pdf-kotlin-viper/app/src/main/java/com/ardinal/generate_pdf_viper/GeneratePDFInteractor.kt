package com.ardinal.generate_pdf_viper

import com.ardinal.generate_pdf_viper.model.PdfContent

class GeneratePDFInteractor : GeneratePDFContracts.GeneratePDFPresentorToInteractorInterface {

    override var presenter: GeneratePDFPresenter? = null

    override fun validateTheData(title: String, desc: String) {
        if (title.isEmpty() || title == "") {
            presenter?.setTitleError()
        }
        if (desc.isEmpty() || desc == "") {
            presenter?.setDescError()
        }
        if (title.isNotEmpty() && desc.isNotEmpty()) {
            presenter?.createObjectSuccess(PdfContent(title, desc))
        }
    }
}
