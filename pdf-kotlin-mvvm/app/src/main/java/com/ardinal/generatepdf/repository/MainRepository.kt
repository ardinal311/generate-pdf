package com.ardinal.generatepdf.repository

import com.ardinal.generatepdf.model.PdfContent

class MainRepository {

    fun createPdfContent(title: String, desc: String): PdfContent {
        var content : PdfContent? = null
        if (title.isNotEmpty() && desc.isNotEmpty()) {
            content = PdfContent(title, desc)
        }
        return content!!
    }
}