package com.ardinal.generatepdf.view

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.design.widget.TextInputEditText
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.ardinal.generatepdf.R
import com.ardinal.generatepdf.model.PdfContent
import com.ardinal.generatepdf.view_model.MainViewModel
import com.itextpdf.text.Document
import com.itextpdf.text.DocumentException
import com.itextpdf.text.Font
import com.itextpdf.text.Paragraph
import com.itextpdf.text.pdf.PdfWriter
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getStoragePermission()

        supportActionBar?.title = getString(R.string.generate_pdf)

        pdf_button.setOnClickListener {
            createNewPdf()
        }
    }

    private fun getStoragePermission() {
        val permission = ArrayList<String>()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.READ_EXTERNAL_STORAGE)
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (permission.isNotEmpty()) {
                ActivityCompat.requestPermissions(this, permission.toTypedArray(), 1)
            }
        }
    }

    private fun getPdfTitle(): String {
        return pdf_title.text.toString()
    }

    private fun getPdfDesc(): String {
        return pdf_desc.text.toString()
    }

    private fun createNewPdf() {

        val viewModel = MainViewModel(
            application = application,
            title =  getPdfTitle(),
            desc = getPdfDesc()
        )

        if (!viewModel.getStatus(getPdfTitle())!!) {
            setFieldError(pdf_title, "Title Masih Kosong!")
        }
        if (!viewModel.getStatus(getPdfDesc())!!) {
            setFieldError(pdf_desc, "Deskripsi Masih Kosong!")
        }
        if (viewModel.getResponseData() != null) {
            generateThePdf(viewModel.getResponseData()!!)
        }
    }

    private fun setFieldError(field: TextInputEditText, message: String) {
        field.error = message
    }

    private fun generateThePdf(content: PdfContent) {
        val dirPath = Environment.getExternalStorageDirectory().absolutePath + "/KOTLIN_PDF/"
        val dirFile = File(dirPath)
        if (!dirFile.exists()) {
            dirFile.mkdirs()
        }

        val targetPdf = dirPath + content.title + ".pdf"
        val filePath = File(targetPdf)
        val doc = Document()

        try {
            val stream = FileOutputStream(filePath)
            PdfWriter.getInstance(doc, stream)
            doc.open()

            doc.addTitle(content.title)
            val largeBold = Font(Font.FontFamily.COURIER, 24f, Font.BOLD)
            doc.add(Paragraph(content.title, largeBold))
            doc.add(Paragraph(content.desc))
            doc.close()

            showMessage("KOTLIN MVVM PDF GENERATED!!")
        } catch (e: IOException) {
            e.printStackTrace()
            showMessage(e.toString())
        } catch (e: DocumentException) {
            e.printStackTrace()
            showMessage(e.toString())
        }

    }

    private fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
