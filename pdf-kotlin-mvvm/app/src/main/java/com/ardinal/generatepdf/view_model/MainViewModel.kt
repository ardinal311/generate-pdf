package com.ardinal.generatepdf.view_model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.ardinal.generatepdf.model.PdfContent
import com.ardinal.generatepdf.repository.MainRepository

class MainViewModel(application: Application, var title: String, var desc: String) : AndroidViewModel(application) {


    fun getResponseData(): PdfContent? {
        val titleStatus = getStatus(title)
        val descStatus = getStatus(desc)

        return if (titleStatus!! && descStatus!!) {
            val repository = MainRepository()
            repository.createPdfContent(title, desc)
        } else {
            null
        }
    }

    fun getStatus(text: String): Boolean? {
        return text.isNotEmpty()
    }
}
