package com.ardinal.generatepdf;

import com.ardinal.generatepdf.model.PdfContent;

public class PdfInteractor implements PdfContract.PresenterToInteractor {

    public PdfPresenter presenter;

    public void setPresenter(PdfPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void createObject(String title, String desc) {

        if (title.isEmpty()) {
            presenter.setTitleError();
        }
        if (desc.isEmpty()) {
            presenter.setDescError();
        }
        if (!title.isEmpty() && !desc.isEmpty()) {
            presenter.createObjectSuccess(new PdfContent(title, desc));
        }
    }
}
