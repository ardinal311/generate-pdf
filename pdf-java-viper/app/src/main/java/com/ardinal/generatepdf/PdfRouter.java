package com.ardinal.generatepdf;

public class PdfRouter implements PdfContract.PresenterToRouter {


    public static final Singleton instance = new Singleton();

    public static final class Singleton {

        private PdfActivity view;

        final void setView(PdfActivity activity) {
            this.view = activity;
        }

        public final void configure(PdfActivity activity) {
            PdfPresenter presenter = new PdfPresenter();
            PdfInteractor interactor = new PdfInteractor();
            PdfRouter router = new PdfRouter();

            activity.setPresenter(presenter);
            presenter.setView(activity);
            presenter.setRouter(router);
            presenter.setInteractor(interactor);
            interactor.setPresenter(presenter);

            this.setView(activity);
        }
    }
}
