package com.ardinal.generatepdf;

import android.app.Activity;

import com.ardinal.generatepdf.model.PdfContent;

class PdfContract {

    interface PresenterToView {
        void setupView();
        void setupLink();
        void setTitleError();
        void setDescError();
        void generatePdf(PdfContent content);
        void showMessage(String message);
    }

    interface PresenterToInteractor {
        void createObject(String title, String desc);
    }

    interface InteractorToPresenter {
        void setTitleError();
        void setDescError();
        void createObjectSuccess(PdfContent content);
    }

    interface ViewToPresenter {

    }

    interface PresenterToRouter {

    }
}
