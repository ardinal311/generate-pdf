package com.ardinal.generatepdf;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ardinal.generatepdf.model.PdfContent;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class PdfActivity extends AppCompatActivity implements PdfContract.PresenterToView {

    private Context context = this;
    private PdfPresenter presenter;

    public void setPresenter(PdfPresenter presenter) {
        this.presenter = presenter;
    }

    private TextInputEditText titleField;
    private TextInputEditText descField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PdfRouter.instance.configure(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getStoragePermission();
        setupView();
    }

    private void getStoragePermission() {
        ArrayList<String> permission = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                permission.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (!permission.isEmpty()) {
                ActivityCompat.requestPermissions(this, permission.toArray(new String[permission.size()]), 1);
            }
        }
    }

    @Override
    public void setupView() {
        getSupportActionBar().setTitle(getString(R.string.generate_pdf));

        titleField = findViewById(R.id.pdf_title);
        descField = findViewById(R.id.pdf_desc);

        setupLink();
    }

    @Override
    public void setupLink() {
        Button submit = findViewById(R.id.pdf_button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateData();
            }
        });
    }

    private void validateData() {
        presenter.interactor.createObject(
                Objects.requireNonNull(titleField.getText()).toString(),
                Objects.requireNonNull(descField.getText()).toString()
        );
    }

    @Override
    public void setTitleError() {
        titleField.setError("Title Masih Kosong");
    }

    @Override
    public void setDescError() {
        descField.setError("Desc Masih Kosong");
    }

    @Override
    public void generatePdf(PdfContent content) {
        String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/JAVA_PDF/";
        File dirFile = new File(dirPath);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }

        String targetPdf = dirPath + content.getTitle() + ".pdf";
        File filePath = new File(targetPdf);
        Document doc = new Document();

        try {
            FileOutputStream stream = new FileOutputStream(filePath);
            PdfWriter.getInstance(doc, stream);
            doc.open();

            doc.addTitle(content.getTitle());
            Font largeBold = new Font(Font.FontFamily.COURIER, 24, Font.BOLD);
            doc.add(new Paragraph(content.getTitle(), largeBold));
            doc.add(new Paragraph(content.getDesc()));
            doc.close();

            showMessage("JAVA VIPER PDF GENERATED");
        }
        catch (IOException e) {
            e.printStackTrace();
            showMessage(e.toString());
        } catch (DocumentException e) {
            e.printStackTrace();
            showMessage(e.toString());
        }
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


}
