package com.ardinal.generatepdf;

import com.ardinal.generatepdf.model.PdfContent;

public class PdfPresenter implements PdfContract.ViewToPresenter, PdfContract.InteractorToPresenter {
    public PdfActivity view;
    public PdfInteractor interactor;
    public PdfRouter router;

    public void setView(PdfActivity view) {
        this.view = view;
    }

    public void setInteractor(PdfInteractor interactor) {
        this.interactor = interactor;
    }

    public void setRouter(PdfRouter router) {
        this.router = router;
    }

    @Override
    public void setTitleError() {
        view.setTitleError();
    }

    @Override
    public void setDescError() {
        view.setDescError();
    }

    @Override
    public void createObjectSuccess(PdfContent content) {
        view.generatePdf(content);
    }

}
