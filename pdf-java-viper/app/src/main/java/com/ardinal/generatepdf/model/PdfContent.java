package com.ardinal.generatepdf.model;

public class PdfContent {
    private String title;
    private String desc;

    public PdfContent() {
    }

    public PdfContent(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public PdfContent getContent() {
        return new PdfContent(getTitle(), getDesc());
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "PdfContent{" +
                "title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
