package com.ardinal.pdf_java_mvp;

import android.text.TextUtils;

public class GeneratePdfInteractor {

    interface OnContentCreated {
        void onTitleError();
        void onDescError();
        void onSuccess(String title, String desc);
    }

    public void validate(final String title, final String desc, final OnContentCreated listener) {
        if (TextUtils.isEmpty(title)) {
            listener.onTitleError();
            return;
        }
        if (TextUtils.isEmpty(desc)){
            listener.onDescError();
        }
        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(desc)) {
            listener.onSuccess(title, desc);
        }
    }
}
