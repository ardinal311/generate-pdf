package com.ardinal.pdf_java_mvp;

import com.ardinal.pdf_java_mvp.model.PdfContent;

class GeneratePdfPresenter implements GeneratePdfInteractor.OnContentCreated {

    private GeneratePdfContract contract;
    private GeneratePdfInteractor interactor;

    GeneratePdfPresenter(GeneratePdfContract contract, GeneratePdfInteractor interactor) {
        this.contract = contract;
        this.interactor = interactor;
    }

    public GeneratePdfContract getContract() {
        return contract;
    }

    public void validateData(String title, String desc) {
        interactor.validate(title, desc, this);
    }

    @Override
    public void onTitleError() {
        contract.setTitleError();
    }

    @Override
    public void onDescError() {
        contract.setDescError();
    }

    @Override
    public void onSuccess(String title, String desc) {
        contract.showData(new PdfContent(title, desc));
    }
}
