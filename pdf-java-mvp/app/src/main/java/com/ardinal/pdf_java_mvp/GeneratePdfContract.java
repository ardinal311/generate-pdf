package com.ardinal.pdf_java_mvp;

import com.ardinal.pdf_java_mvp.model.PdfContent;

interface GeneratePdfContract {

    void validateData();

    void setTitleError();

    void setDescError();

    void showData(PdfContent content);

    void showMessage(String message);
}
